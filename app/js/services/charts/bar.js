
angular.module('TuwasApp.services.charts')
  .factory('BarChart', function (Chart, IndicatorGroups, Analytics, Utils) {
    var baseChart = Chart(),
      indicatorGroups = [],
      currentIndicatorGroup = {},
      dataIndicators = [],
      currentDataIndicator = {},
      barChart;

    function loadDataIndicators() {
      if (Utils.isValidId(currentIndicatorGroup.id)) {
        //Get one DataElementGroup and get update it's data elements
        IndicatorGroups.get(currentIndicatorGroup.id).then(function (data) {
          barChart.setDataIndicators(data.indicators);
        }, function (response) {
          //TODO Display error message
        });
      }
    }

    barChart = {
      init: function (callback) {
        baseChart.setDrawCallback(callback);

        if (indicatorGroups.length === 0) {
          IndicatorGroups.getList().then(function (response) {
            barChart.setIndicatorGroups(response.indicatorGroups);
          });
        }
      },

      updateData: function () {
        if (Utils.isValidId(currentDataIndicator.id)) {
          Analytics.reset();
          Analytics.addDimension(currentDataIndicator.id, 'dx');
          Analytics.addDimension('USER_ORGUNIT_CHILDREN', 'ou');

          Analytics.addFilter(this.getTimePeriod(), 'pe');

          Analytics.getList().then(function (data) {
            baseChart.setGraphData(data);
            baseChart.getDrawCallback()(data);
          }, function (response) {
            //TODO Display error message
            console.log(response);
          });
        }
      },

      setIndicatorGroups: function (idcg) {
        indicatorGroups = idcg;
      },
      getIndicatorGroups: function () {
        return indicatorGroups;
      },
      setCurrentIndicatorGroup: function (idg) {
        currentIndicatorGroup = idg;
        currentDataIndicator = undefined;

        loadDataIndicators();
      },
      getCurrentIndicatorGroup: function () {
        return currentIndicatorGroup;
      },
      setDataIndicators: function (di) {
        dataIndicators = di;
      },
      getDataIndicators: function () {
        return dataIndicators;
      },
      setCurrentDataIndicator: function (cdi) {
        currentDataIndicator = cdi;
      },
      getCurrentDataIndicator: function () {
        return currentDataIndicator;
      }
    }

    return _.extend(baseChart, barChart);
  });
