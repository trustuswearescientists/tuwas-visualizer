angular.module('TuwasApp.services.charts')
  .factory('LineChart', function (DataElementChart, Analytics) {
    var lineChart = DataElementChart();

    lineChart.updateData = function () {
      if (this.getCurrentDataElement().id != null && this.getCurrentDataElement().id != undefined) {
        Analytics.reset();
        Analytics.addDimension(this.getCurrentDataElement().id, 'dx');
        Analytics.addDimension(this.getTimePeriod(), 'pe');

        Analytics.getList().then(function (data) {
          lineChart.setGraphData(data);
          lineChart.getDrawCallback()(lineChart.getGraphData());
        }, function (response) {
          //TODO Display error message
          console.log(response);
        });
      }
    }

    return lineChart;
  });
