'use strict';

angular.module('TuwasApp.services', ['ngResource', 'restangular'])
    .factory('Indicators', function (Restangular) {
        return Restangular.all('indicators');
    })
    .factory('IndicatorGroups', function (Restangular) {
        return Restangular.all('indicatorGroups');
    })
    .factory('DataElementGroups', function (Restangular) {
        return Restangular.all('dataElementGroups');
    })
    .factory('OrganisationUnitGroupSets', function (Restangular) {
        return Restangular.all('dimensions');
    })
    .factory('Analytics', function (Restangular) {
        var api_analytics = Restangular.all('analytics'),
            dimensions = [],
            filters = []

        return {
            /**
             * Add a dimension to the analytics call that will be sent to DHIS2
             *
             * @param id Dimension id (This is the id that is given to the DHIS2 dimension
             * @param type Prefix for the dimension to indicate the type of dimension
             */
            addDimension: function (id, type) {
                var dimension = {
                    'id': id
                };

                //Check if we're dealing with a type or an array of subtypes
                if (angular.isString(arguments[1])) {
                    dimension.type = arguments[1];
                } else if (angular.isArray(arguments[1]) && arguments[1].length > 0) {
                    dimension.sub_dimensions_ids = arguments[1];
                }
                dimensions[type] = dimension;
            },

            /**
             * Add a filter that will be used for filtering the data returned by DHIS2 api
             *
             * @param filter_id Id of the item to filter on
             */
            addFilter: function (filter_id) {
                var filter = {
                    'id': filter_id
                }

                //Check if we're dealing with a type or an array of subtypes
                if (angular.isString(arguments[1])) {
                    filter.type = arguments[1];
                } else if (angular.isArray(arguments[1]) && arguments[1].length > 0) {
                    filter.sub_filter_ids = arguments[1];
                }
                filters[filter_id] = filter;
            },

            /**
             * Return a Restangular promise for a dataset based on the set dimensions and filters
             *
             * @returns {Restangular|getList} Result based on the dimensions that were added through
             *          the addDimension and addFilter methods
             */
            getList: function () {
                return api_analytics.getList({'dimension': prepareDimensionsForQuery(), 'filter': prepareFiltersForQuery()});
            },

            /**
             * Resets the filters and dimensions for this analytics object
             */
            reset: function () {
                dimensions = [];
                filters = [];
            }
        };

        /**
         * Prepares the dimensions to be used in the query string
         * It prefixes the dimensions with their type like the dhis api expects them
         *
         * @see dhis manual for more information on how to format filters for the API
         *
         * @returns {Array}
         */
        function prepareDimensionsForQuery() {
            var index,
                query_dimensions = [],
                dimension_string = '';

            for (index in dimensions) {
                var dimension = dimensions[index];

                if (dimension.hasOwnProperty('type'))
                    dimension_string = dimension.type + ':' + dimension.id;
                else if (dimension.hasOwnProperty('sub_dimensions_ids')) {
                    var i = 0;
                    var count = dimension.sub_dimensions_ids.length;
                    dimension_string = dimension.id + ':' + dimension.sub_dimensions_ids[i].id;

                    //TODO: The following loop looks like a mess... clean it up :)
                    while (count > 1) {
                        i++;
                        dimension_string += ";" + dimension.sub_dimensions_ids[i].id;
                        count--;
                    }
                }

                if (dimension_string !== '') {
                    query_dimensions.push(dimension_string);
                }
            }
            return query_dimensions;
        };

        /**
         * Prepares filters to be used in the query string. It returns an array with filter values
         * Sub elements are added to the filter id as a suffix
         *
         * @see dhis manual for more information on how to format filters for the API
         *
         * @returns {Array}
         */
        function prepareFiltersForQuery() {
            var index,
                query_filters = [];

            for (index in filters) {
                var filter = filters[index],
                    filter_string = '';

                if (filter.hasOwnProperty('type')) {
                    filter_string = filter.type + ':' + filter.id;
                } else if (filter.hasOwnProperty('sub_filter_ids')) {
                    filter_string = filter.id + ':' + filters[index].sub_filter_ids.join(',');
                }

                if (filter_string !== '') {
                    query_filters.push(filter_string);
                }
            }
            return query_filters;
        }
    })
    /**
     * The time intervals as they are used by DHIS2
     * Attempted to order them in a logical manner
     */
    .factory('TimeIntervals', function () {
        return [
          "LAST_WEEK",
          "LAST_4_WEEKS",
          "LAST_12_WEEKS",
          "LAST_52_WEEKS",

          "LAST_MONTH",
          "LAST_3_MONTHS",
          "LAST_12_MONTHS",
          "MONTHS_THIS_YEAR",
          "MONTHS_LAST_YEAR",

          "LAST_QUARTER",
          "LAST_4_QUARTERS",
          "QUARTERS_THIS_YEAR",
          "QUARTERS_LAST_YEAR",

          "LAST_BIMONTH",
          "LAST_6_BIMONTHS",

          "LAST_SIX_MONTH",
          "LAST_2_SIXMONTHS",

          "THIS_FINANCIAL_YEAR",
          "LAST_FINANCIAL_YEAR",
          "LAST_5_FINANCIAL_YEARS",

          "THIS_YEAR",
          "LAST_YEAR",
          "LAST_5_YEARS"
        ];
    })
    /**
     * The selected time intervals
     * Default this is set to the three intervals set below
     */
    .factory('SelectedTimeIntervals', function () {
        return [
          "LAST_MONTH",
          "LAST_12_MONTHS",
          "LAST_3_MONTHS"
        ];
    })
    .value('version', '0.1');
