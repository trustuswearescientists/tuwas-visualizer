'use strict';

angular.module('TuwasApp.services', ['ngResource', 'restangular'])
  .factory('DhisJsonpCall', function ($resource) {
    var dhis_base_url = 'http://apps.dhis2.org/demo/api/';

      /**
       * Returns a new TuwasPromise object
       *
       * Construction of this object looks a tad weird but it's a simple way to restrict new to the creator function
       * and keep the 'Type' of the object to be a TuwasPromise object this way we don't have to use new when creating
       * a TuwasPromise but can just call TuwasPromise() to get an instance of the object.
       *
       * @returns {TuwasPromise}
       * @constructor
       */
        var TuwasPromise = function() {
          var jsonp_result = undefined,
              then_callback = undefined;

          var TuwasPromise = function () {}
          TuwasPromise.prototype = {
            set_response: function (response) {
              jsonp_result = response;
              if (typeof then_callback === 'function') {
                then_callback(jsonp_result);
              }
            },

            then: function (callback) {
              then_callback = callback;
              if (typeof jsonp_result !== 'undefined')
                then_callback(jsonp_result);
            }
          }

          return new TuwasPromise();
        };

    /**
     * Returns a Restangular like object with the methods that we currently use
     *
     * getList:
     * Handles the ajax call to DHIS2 and returns a promise to be used with this result.
     *
     * get:
     * //TODO also does something
     */
    return function (resource_name) {
      return {
        getList: function (params) {
          var promise = TuwasPromise(),
              query_params = [];

          $resource(dhis_base_url + resource_name + '.jsonp', function () {
            return _.extend({ callback:'JSON_CALLBACK' }, params);
          }(),
            { get: { method:'JSONP' } }
          ).get(function (response) {
              promise.set_response(response);
          });
          return promise;
        },
        get: function (resource_id) {
          var promise = TuwasPromise();

          $resource(dhis_base_url + resource_name + '/' + resource_id + '.jsonp',
            { callback:'JSON_CALLBACK' },
            { get: { method:'JSONP' } }
          ).get(function (response) {
              promise.set_response(response);
            });
          return promise;
        }
      };
    }
  })
  .factory('Indicators', function (DhisJsonpCall) {
    return DhisJsonpCall('indicators');
  })
  .factory('IndicatorGroups', function (DhisJsonpCall) {
    return DhisJsonpCall('indicatorGroups');
  })
  .factory('DataElementGroups', function (DhisJsonpCall) {
    return DhisJsonpCall('dataElementGroups');
  })
  .factory('OrganisationUnitGroupSets', function (DhisJsonpCall) {
    return DhisJsonpCall('dimensions');
  })
  .factory('Analytics', function (DhisJsonpCall) {
    var api_analytics =  DhisJsonpCall('analytics'),
        dimensions = [],
        filters = []

    return {
      /**
       * Add a dimension to the analytics call that will be sent to DHIS2
       *
       * @param id Dimension id (This is the id that is given to the DHIS2 dimension
       * @param type Prefix for the dimension to indicate the type of dimension
       */
      addDimension: function (id, type) {
        var dimension = {
          'id': id
        };
        
        //Check if we're dealing with a type or an array of subtypes
        if (angular.isString(arguments[1])) {
          dimension.type = arguments[1];
        } else if (angular.isArray(arguments[1]) && arguments[1].length > 0) {
          dimension.sub_dimensions_ids = arguments[1];
         // alert(dimension.sub_dimensions_ids[0].id);
       //   alert(dimension.sub_dimensions_ids[1].id);
        }
        dimensions[type] = dimension;
      },

      /**
       * Add a filter that will be used for filtering the data returned by DHIS2 api
       *
       * @param filter_id Id of the item to filter on
       * @param sub_filter_ids Array of Id's of sub items to the filter_id
       */
      addFilter: function (filter_id) {
        var filter = {
          'id': filter_id
        }

        //Check if we're dealing with a type or an array of subtypes
        if (angular.isString(arguments[1])) {
          filter.type = arguments[1];
        } else if (angular.isArray(arguments[1]) && arguments[1].length > 0) {
          filter.sub_filter_ids = arguments[1];
        }
        filters[filter_id] = filter;
      },
      /**
       * Return a Restangular promise for a dataset based on the set dimensions and filters
       *
       * @returns {Restangular|getList} Result based on the dimensions that were added through
       *          the addDimension and addFilter methods
       */
      getList: function () {
        return api_analytics.getList({'dimension': prepareDimensionsForQuery(), 'filter': prepareFiltersForQuery()});
      },

      reset: function () {
        dimensions = [];
        filters = [];
      }
    };

    function prepareDimensionsForQuery() {
      var index,
		query_dimensions = [],
		dimension_string  = '';
      
      for (index in dimensions) {
    	  var dimension = dimensions[index];
    	 
          if (dimension.hasOwnProperty('type')) 
        	  dimension_string = dimension.type + ':' + dimension.id;
          else if (dimension.hasOwnProperty('sub_dimensions_ids')){
        	  var i =0;
        	  var count = dimension.sub_dimensions_ids.length;
          	 dimension_string= dimension.id + ':' + dimension.sub_dimensions_ids[i].id;
          	  while (count>1){
          		  i++;
          		  dimension_string += ";" + dimension.sub_dimensions_ids[i].id;
          		  count--;
          	  }
       
          }

          if (dimension_string !== '') {
        	  query_dimensions.push(dimension_string);
          }
      }
      return query_dimensions;
    };

    function prepareFiltersForQuery() {
      var index,
        query_filters = [],
        filter_string = '';

      for (index in filters) {
        var filter = filters[index];

        if (filter.hasOwnProperty('type')) {
          filter_string = filter.type + ':' + filter.id;
        } else if (filter.hasOwnProperty('sub_filter_ids')) {
          filter_string = filter.id + ':' + filters[index].sub_filter_ids.join(',');
        }

        if (filter_string !== '') {
          query_filters.push(filter_string);
        }
      }
      return query_filters;
    }
  })
    .factory('TimeIntervals', function () {
      return [
        "LAST_WEEK",
        "LAST_4_WEEKS",
        "LAST_12_WEEKS",
        "LAST_52_WEEKS",

        "LAST_MONTH",
        "LAST_3_MONTHS",
        "LAST_12_MONTHS",
        "MONTHS_THIS_YEAR",
        "MONTHS_LAST_YEAR",

        "LAST_QUARTER",
        "LAST_4_QUARTERS",
        "QUARTERS_THIS_YEAR",
        "QUARTERS_LAST_YEAR",

        "LAST_BIMONTH",
        "LAST_6_BIMONTHS",

        "LAST_SIX_MONTH",
        "LAST_2_SIXMONTHS",

        "THIS_FINANCIAL_YEAR",
        "LAST_FINANCIAL_YEAR",
        "LAST_5_FINANCIAL_YEARS",

        "THIS_YEAR",
        "LAST_YEAR",
        "LAST_5_YEARS"
      ];
    })
    .factory('SelectedTimeIntervals', function () {
        return ["LAST_MONTH",
            "LAST_12_MONTHS",
            "LAST_3_MONTHS"];
    })
  
  
  .value('version', '0.1');
