'use strict';

angular.module('TuwasApp.utils', [])
  .factory('Utils', function () {
    return {
      /**
       * Check if the given id is a valid 11 character string
       *
       * @param id_to_check {String}
       * @returns {boolean}
       */
     isValidId: function (id_to_check) {
        if (typeof id_to_check === 'string' && id_to_check.length === 11)
          return true;
        return false;
     },

      /**
       * Capitalize the first letter of a string
       *
       * @param string {string}
       * @returns {string}
       */
      capitaliseFirstLetter: function (string)
      {
        return string.charAt(0).toUpperCase() + string.slice(1);
      }

    };
  })

  /**
   * A Toggle object that supports a callback when the toggle method is called
   * callback will need to be set upon creation of the Toggle
   *
   * Toggle.toggle supports a force parameter to force the status to either true|false
   * Toggle.status is used to get the current state of the Toggle
   */
  .factory('Toggle', function () {
    return function (callback) {
      var status = false;
      return {
        /**
         * Changes to status of the toggle to the inverted unless the force param is set
         *
         * @param force {boolean} Forces the status of the toggle to be what the force param is
         * @returns {boolean} Returns the new status
         */
        toggle: function (force) {
          if (typeof  force === 'boolean') {
            status = force;
          } else {
            status = ! status;
          }
          if (typeof callback === 'function') {
            callback();
          }
          return status;
        },
        /**
         * Returns the current status
         *
         * @returns {boolean}
         */
        status: function () {
          return status;
        }
      }
    };
  });
