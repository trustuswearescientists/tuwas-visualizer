'use strict';

angular.module('TuwasApp.controllers').
    controller('SettingsController', ['$scope', 'TimeIntervals','SelectedTimeIntervals', function ($scope, TimeIntervals, SelectedTimeIntervals) {
        // change this number to select number of intervals
        $scope.maxNumberOfIntervals = 3;

        $scope.timeIntervals = TimeIntervals;

        $scope.selectedTimeIntervals = SelectedTimeIntervals;

        $scope.isCurrentlySelected = function (interval) {
          return SelectedTimeIntervals.indexOf(interval) > -1;
        }

        $scope.toggleSelection = function(interval){
            var idx = $scope.selectedTimeIntervals.indexOf(interval);
            // is currently selected
            if (idx > -1) {
                $scope.selectedTimeIntervals.splice(idx, 1);
            }
            // is newly selected
            else {
                $scope.selectedTimeIntervals.push(interval);
            }
        }

        $scope.maxChosen = function(interval){
            var idx = $scope.selectedTimeIntervals.indexOf(interval)
            if (idx > -1) {
                return false;
            }
            return $scope.selectedTimeIntervals.length >= $scope.maxNumberOfIntervals;
        }
    }]);
