'use strict';
angular.module('TuwasApp.controllers').
  controller('LinearGraphController', ['$scope', 'LineChart', 'Toggle',
    function ($scope, LineChart, Toggle) {
      $scope.settings.lastChart = 'line';
      $scope.lineChart = LineChart;
      LineChart.init(mapGraphData);

      // Check if we have cached graph data and display it if we do
      if (LineChart.getGraphData()) {
        mapGraphData(LineChart.getGraphData());
      }

      $scope.pickElement = Toggle();
      $scope.pickElementGroup = Toggle();

      //For the swipe at left direction
      $scope.previousTime = function () {
        try {
          LineChart.updateDataLeft();
        } catch(error) {
          $scope.messages.addMessage(error.message, 'error');
        }
      }

      //For the swipe at right direction
      $scope.nextTime = function () {
        try {
          LineChart.updateDataRight();
        } catch(error) {
          $scope.messages.addMessage(error.message, 'error');
        }
      }

      $scope.updateDataElgroup = function (dataElementGroup) {
        LineChart.setCurrentDataElementGroup(dataElementGroup);

        $scope.pickElementGroup.toggle();
        $scope.pickElement.toggle(true);
      };

      $scope.updateDataElement = function (dataElement) {
        LineChart.setCurrentDataElement(dataElement);

        $scope.pickElement.toggle();

        try {
          LineChart.updateData();
        } catch(error) {
          $scope.messages.addMessage(error.message, 'error');
        }
      }

      function mapGraphData(data) {
        var key = LineChart.getCurrentDataElement().name;
        var values = [];

        if (data.rows[0] == null || data.rows[0] == undefined) {
          //Display a message to the user with the status
          $scope.messages.addMessage('There is no data for the selected values :(', 'warning');

          //Show the manual
          LineChart.isManualVisible = true;

          d3.select("#chart svg")
            .text("");
          return null;
        }

        //Hide the manual
        LineChart.isManualVisible = false;

        for (var i = 0; i < data.rows.length; i++) {
          var row = data.rows[i];
          var date = new Date(Date.parse(row[1].substring(0, 4) + "/" + row[1].substring(4, 6) + "/01"));

          values.push({
            x: date,
            y: Math.abs(row[2])
          });
        }
        values.sort(
          function (a, b) {
            return a.x - b.x;
          }

        );

        var graphData = [
          {
            "key": key,
            "values": values
          }
        ];

        redraw(graphData);
      }

      $scope.$on('beforeUpdate.directive', function (event) {

      });

      function redraw(graphData) {
        nv.addGraph(function () {
          var chart = nv.models.lineChart()
            .x(function (d) {
              return d.x
            })
            .y(function (d) {
              return d.y
            })
            .color(d3.scale.category10().range());

          //chart.margin({left: 100});
          // chart.forceY([ ,max]);
          chart.xAxis
            .tickFormat(function (d) {
              return d3.time.format('%x')(new Date(d))
            });


          d3.select('#chart svg')
            .attr('width', 30)
            .attr('height', 21)
            .datum(graphData)
            .transition().duration(500)
            .call(chart);

          nv.utils.windowResize(chart.update);

        });


      }


    }]);
