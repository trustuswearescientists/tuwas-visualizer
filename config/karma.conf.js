module.exports = function(config){
    config.set({
    basePath : '../',

    files : [
      'app/lib/angular/angular.js',
      'app/lib/angular/angular-*.js',
      'test/lib/angular/angular-mocks.js',
      'app/lib/bower/jquery/jquery.js',
      'app/lib/bower/jasmine-jquery/lib/jasmine-jquery.js',
      'app/lib/bower/lodash/dist/lodash.js',
      'app/lib/bower/restangular/dist/restangular.js',
      'app/js/**/*.js',
      'test/unit/**/*.js',

      //fixtures
      { pattern: 'test/fixtures/*.json', watched: true, served: true, included: false }
    ],

    autoWatch : true,

    frameworks: ['jasmine'],

    browsers : ['Chrome'],

    plugins : [
            'karma-junit-reporter',
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine'       
            ],

    //TODO this was only added because test runs fail, and i found this fix here https://github.com/angular/angular-seed/issues/110 
    exclude: ['app/lib/angular/angular-loader*.js'],

    junitReporter : {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }

})};
